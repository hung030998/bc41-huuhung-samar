var testimonialThumbs = new Swiper(".testimonial__image", {
    spaceBetween: 10,
    slidesPerView: 3,
    centeredSlides: true,
    freeMode: true,
    autoplay: true,
    loop: true,
    speed: 3000
  });
  var testimonialContent = new Swiper(".testimonial__text", {
    spaceBetween: 10,
    autoplay: true,
    loop: true,
    speed: 3000,
    thumbs: {
      swiper: testimonialThumbs
    },
    pagination: {
      el: ".swiper-pagination",
      clickable: true
    }
  });


//owl

$(document).ready(function(){
  $('.owl-carousel').owlCarousel({
    loop:true,
    margin:30,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:3
        }
    }
})
});


function scrollToTop(idClick, idTop){
  let click = document.querySelector(idClick)
  let top = document.querySelector(idTop)
  click.addEventListener("click",function(){
    top.scrollIntoView({
        behavior: "smooth", 
        block: "end", 
        inline: "nearest"
      })
  })
}

scrollToTop(".scroll--top",".header")